using UnityEngine.SceneManagement;
using UnityEngine;

public class GameStateChanger : MonoBehaviour
{
    // ��������� � ������ ������
    // �������� ���� ������
    [SerializeField] private AudioClip _winClip;

    // �������� ���� ���������
    [SerializeField] private AudioClip _loseClip;

    // �������� �����
    private AudioSource _audioSource;
    private const string LevelKey = "Level";

    // ����� ������
    private static int _level;

    // ������ �������� ���������
    private CharacterHealth _playerHealth;

    // ������ ��������� ������
    private EnemySpawner _enemySpawner;

    // ������ ������� ����
    private Screen[] _screens;
    // Start is called before the first frame update
    public static int Level
    {
        get
        {
            // ���� ����� ������ <= 0
            if (_level <= 0)
            {
                // ����������� _level �������� ������� ������
                // ����������� � PlayerPrefs � ������ LevelKey
                _level = PlayerPrefs.GetInt(LevelKey, 1);
            }
            // ���������� ����� ������
            return _level;
        }
        set
        {
            // ����� ����� ������
            _level = value;

            // ��������� _level � PlayerPrefs � ������ LevelKey
            PlayerPrefs.SetInt(LevelKey, _level);
        }
    }

    public void NextLevel()
    {
        // ����������� ����� ������ �� 1
        Level++;

        // ��������� �����
        LoadScene();
    }

    public void RestartLevel()
    {
        // ��������� �����
        LoadScene();
    }

    // ������������ �� ������ �������
    public void DropLevel()
    {
        // ����� ����� ������ 1
        Level = 1;

        // ��������� �����
        LoadScene();
    }
    private void LoadScene()
    {
        // ���������� ����� ��������
        ShowScreen<LoadingScreen>();

        // ��������� ����� � �������� 0
        // ��� ������������� ������ 1
        // ��� ��� � ����� ���� ���� ���� �������
        SceneManager.LoadSceneAsync(0);
    }

    private void Start()
    {
        // �������� ����� Init()
        Init();
    }

    private void Init()
    {
        // �������� ��������� AudioSource
        _audioSource = GetComponentInChildren<AudioSource>();
        // ������� ������ ���� PlayerHealth
        // ����������� ��� _playerHealth
        _playerHealth = FindObjectOfType<PlayerHealth>();

        // ������� ������ ���� EnemySpawner
        // ����������� ��� _enemySpawner
        _enemySpawner = FindObjectOfType<EnemySpawner>();

        // ������� ������� ���� Screen
        // ���������� �� � ������ _screens
        _screens = FindObjectsOfType<Screen>(true);

        // ������������ ������� OnDie
        // �������� ����� LoseGame()
        _playerHealth.OnDie += LoseGame;

        // ������������ ������� OnAllEnemiesDie
        // �������� ����� WinGame()
        _enemySpawner.OnAllEnemiesDie += WinGame;

        // ���������� ������� �����
        ShowScreen<GameScreen>();
    }
    private void LoseGame()
    {
        // ���������� ����� ���������
        ShowScreen<GameLoseScreen>();
        _audioSource.PlayOneShot(_loseClip);
    }

    private void WinGame()
    {
        // ���������� ����� ��������
        ShowScreen<GameWinScreen>();
        _audioSource.PlayOneShot(_winClip);
    }

    private void ShowScreen<T>() where T : Screen
    {
        // �������� �� ���� �������
        for (int i = 0; i < _screens.Length; i++)
        {
            // ���� ����� ������������� ���� T
            // ������ ��� ��������, ����� � ����������
            _screens[i].SetActive(_screens[i] is T);
        }
    }

    private void OnDestroy()
    {
        // ���� ��� ���� ������ �������� ������
        if (_playerHealth)
        {
            // ������������ �� ������� OnDie
            _playerHealth.OnDie -= LoseGame;
        }
        // ���� ��� ���� ������ ��������� ������
        if (_enemySpawner)
        {
            // ������������ �� ������� OnAllEnemiesDie
            _enemySpawner.OnAllEnemiesDie -= WinGame;
        }
    }
}
