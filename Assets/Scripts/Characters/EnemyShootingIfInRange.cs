// ������� ������������ �� EnemyShooting
public class EnemyShootingIfInRange : EnemyShooting
{
    // �������������� ����� Shooting()
    protected override void Shooting()
    {
        // ���� ���� �������� � ���� ���� � ������
        if (CheckTargetInRange() && CheckHasBulletsInRow())
        {
            // �������� �� ����
            Shoot();
        }
    }
}