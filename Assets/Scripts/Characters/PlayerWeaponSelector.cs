// ���������� ��������� (�������)
using System.Collections.Generic;

// ���������� ������� Unity
using UnityEngine;

public class PlayerWeaponSelector : CharacterWeaponSelector
{
    // ������ ������� _weaponKeyToIdentityPairs
    // � ��� ������ ����� ������ �� ����������
    // � ��������� � ��� ������
    Dictionary<KeyCode, WeaponIdentity> _weaponKeyToIdentityPairs = new Dictionary<KeyCode, WeaponIdentity>()
    {
        // ������ 1 ������������� ��������
        { KeyCode.Alpha1, WeaponIdentity.Rifle },

        // ������ 2 ������������� ��������
        { KeyCode.Alpha2, WeaponIdentity.Pistol },

        // ������ 3 ������������� ��������
        { KeyCode.Alpha3, WeaponIdentity.Shotgun }
    };

    private void Update()
    {
        // ������ ���� �������� ����� CheckChangeKey()
        CheckChangeKey();
    }

    private void CheckChangeKey()
    {
        // �������� �� ���� ����� � �������
        foreach (var pair in _weaponKeyToIdentityPairs)
        {
            // ���� ������ ������, ������� ������������� ������ �� ������
            if (Input.GetKeyDown(pair.Key))
            {
                // ������������� ��������� ������
                _weaponId = pair.Value;

                // �������� ����� RefreshSelectedWeapon()
                // ������� ����� � ������������ ������
                RefreshSelectedWeapon();
            }
        }
    }
}