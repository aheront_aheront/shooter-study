// ���������� �������
using System;

// ���������� ���������
using System.Collections.Generic;

// �������� � ��������� Unity
using UnityEngine;

// �������� �� ���������� ����������
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    // ����������� ������� � �������� ��������� ������
    private const float MinViewportPosition = -0.1f;

    // ������������ ������� � �������� ��������� ������ 
    private const float MaxViewportPosition = 1.1f;

    // ������ ����� ��� ��������� ������
    [SerializeField] private Character[] _enemyPrefabs;

    // ���������� ������, ������� ����� �������
    [SerializeField] private int _enemyCountByLevel = 2;

    // �������� ����� ��������� ������
    [SerializeField] private float _spawnDelay = 1f;
    // ������ �������� �������� ������
    private List<CharacterHealth> _enemiesHealth = new List<CharacterHealth>();

    // ������� ������ ���� ������
    public Action OnAllEnemiesDie;

    // ������ �����, � ������� ����� ��������� �����
    private EnemySpawnPoint[] _spawnPoints;

    // �������� ������ ����
    private Camera _mainCamera;

    // ���������� ��� ��������� ������
    private int _spawnedEnemyCount;

    // ������ ��� ������������ ��������
    private float _spawnTimer;

    // �������, ������� ���������� ��� �������� �����
    public Action<Character> OnSpawnEnemy;

    private void Start()
    {
        // �������� ����� Init()
        Init();
    }

    private void Init()
    {
        // ��������� ������ _spawnPoints ���������
        // ���� EnemySpawnPoint, ������� ������������ �� �����
        _spawnPoints = FindObjectsOfType<EnemySpawnPoint>();

        // ����������� _mainCamera ������ ������
        _mainCamera = Camera.main;
    }
    private void Update()
    {
        // ���������� ����� ������
        SpawnEnemies();
    }

    private void SpawnEnemies()
    {
        // ���� ������� �������� ���������� ������
        // ���� ����� ����������� ������ >= ����� ������ �� ���� �������
        if (_spawnedEnemyCount >= GameStateChanger.Level * _enemyCountByLevel)
            // ��������� ������ �������� ������
            // �� �����, ��������� � ���������� �����
            _spawnTimer -= Time.deltaTime;

        // ���� ������ ������ ����
        if (_spawnTimer <= 0)
        {
            // ������ ������ �����
            SpawnEnemy();

            // ���������� �������� �������
            ResetSpawnTimer();
        }
    }
    private void RemoveEnemy(CharacterHealth health)
    {
        // ������� ������ �� ������ ��������
        _enemiesHealth.Remove(health);

        // ������������ �� ������� OnDieWithObject
        health.OnDieWithObject -= RemoveEnemy;

        // ���� �������� �������� ������ <= 0
        if (_enemiesHealth.Count <= 0)
        {
            // �������� ������� OnAllEnemiesDie
            OnAllEnemiesDie?.Invoke();
        }
    }
    private void SpawnEnemy()
    {
        EnemySpawnPoint spawnPoint = GetRandomSpawnPoint();
        Character newEnemy = Instantiate(GetRandomEnemyPrefab(), spawnPoint.transform.position, Quaternion.identity);
        _spawnedEnemyCount++;

        // �����: �������� ��������� ��������
        // ���������� ������������ �����
        CharacterHealth newHealth = newEnemy.GetComponent<CharacterHealth>();

        // �����: ������������ ������� OnDieWithObject
        // �������� ����� RemoveEnemy()
        newHealth.OnDieWithObject += RemoveEnemy;

        // �����: ��������� � ������ �������� ������ �����
        _enemiesHealth.Add(newHealth);

        OnSpawnEnemy?.Invoke(newEnemy);
    }
    private Character GetRandomEnemyPrefab()
    {
        // ���������� ��������� ������ �����
        return _enemyPrefabs[Random.Range(0, _enemyPrefabs.Length)];
    }

    private EnemySpawnPoint GetRandomSpawnPoint()
    {
        // �������� ������ ��������� ����� ��������� ������
        // ������� ��������� �� ��������� ��������� ������
        List<EnemySpawnPoint> possiblePoints = GetSpawnPointsOutOfCamera();

        // ���� ���� ���� �� ���� ��������� �����
        if (possiblePoints.Count > 0)
        {
            // ���������� ��������� ����� �� ������ ���������
            return possiblePoints[Random.Range(0, possiblePoints.Count)];
        }
        // ���������� ��������� ����� �� ������� ���� �����
        return _spawnPoints[Random.Range(0, _spawnPoints.Length)];
    }
    private List<EnemySpawnPoint> GetSpawnPointsOutOfCamera()
    {
        // ������ ����� ������ ��������� ����� ��������� ������
        List<EnemySpawnPoint> possiblePoints = new List<EnemySpawnPoint>();

        // ������� ���������� ��� �������� ������� �����
        Vector3 pointViewportPosition;

        // �������� �� ���� ������
        for (int i = 0; i < _spawnPoints.Length; i++)
        {
            // �������� ������� ����� �� Viewport-�����������
            pointViewportPosition = _mainCamera.WorldToViewportPoint(_spawnPoints[i].transform.position);

            // ���� ����� ��������� �� ��������� ��������� ������
            if (pointViewportPosition.x >= MinViewportPosition && pointViewportPosition.x <= MaxViewportPosition
                && pointViewportPosition.y >= MinViewportPosition && pointViewportPosition.y <= MaxViewportPosition)
            {
                // ���������� � � ��������� � ��������� �����
                continue;
            }
            // ��������� � � ������ ��������� �����
            possiblePoints.Add(_spawnPoints[i]);
        }
        // ���������� ������ ��������� �����
        return possiblePoints;
    }
    private void ResetSpawnTimer()
    {
        // ������������ �������� ������� � ��������
        _spawnTimer = _spawnDelay;
    }

}
