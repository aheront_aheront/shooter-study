using System;
using UnityEngine;
using UnityEngine.Animations.Rigging;

// ������� ����� �����������
public abstract class CharacterShooting : CharacterPart, IWeaponDependent
{
    // ��������� � ������ ����������� ������
    private const string IsReloadingKey = "IsReloading";

    // ���������� � ��������� �������� ���������
    private Rig _rig;
    // ������� ��������� �������� ������
    public Action<Weapon> OnSetCurrentWeapon;
    // ��������� � ������ ������ ������
    private const string WeaponIdKey = "WeaponId";

    private WeaponIdentity _weaponId;

    // �������� ��������
    private Animator _animator;

    // ������ ����� ������
    private Weapon[] _weapons;

    // ������� ������
    private Weapon _currentWeapon;
    // ����������� ����
    public const float DefaultDamageMutiplier = 1;

    // ��������� �����
    private float _damageMultiplier = DefaultDamageMutiplier;

    // ������������ ����������� �����
    private float _damageMultiplierDuration;

    // ������ ����������� �����
    private float _damageMultiplierTimer;

    // ������� ��������� ��������� �����
    public Action<float> OnSetDamageMutiplier;

    // ������� ��������� ������� �����
    public Action<float, float> OnChangeDamageTimer;

    // ��������, ����� ������ ������� ��������� �����
    public float DamageMultiplier => _damageMultiplier;
    // �������� �� ����
    protected abstract void Shooting();

    // ������������ ������
    protected abstract void Reloading();

    public void SetDamageMultiplier(float multiplier, float duration)
    {
        // ����� ��������� �����
        _damageMultiplier = multiplier;

        // ���������� ������������ �����
        _damageMultiplierDuration = duration;

        // �������� ������ �����
        _damageMultiplierTimer = 0;

        // �������� ������� OnSetDamageMutiplier
        // ������� � ���� ��������� �����
        OnSetDamageMutiplier?.Invoke(_damageMultiplier);

        // �������� ������� OnChangeDamageTimer
        // ������� � ���� ������ � ������������ �����
        OnChangeDamageTimer?.Invoke(_damageMultiplierTimer, _damageMultiplierDuration);
    }
    protected override void OnInit()
    {
        _animator = GetComponentInChildren<Animator>();

        // �����: �������� ��������� Rig
        _rig = GetComponentInChildren<Rig>();

        _weapons = GetComponentsInChildren<Weapon>(true);

        InitWeapons(_weapons);
        SetDefaultDamageMultiplier();
    }

    private void Update()
    {
        // ���� �������� �� �������
        if (!IsActive)
        {
            // ������� �� ������
            return;
        }
        // �������� ����� Shooting()
        Shooting();

        // �������� ����� Reloading()
        Reloading();

        // �������� ����� DamageBonusing()
        DamageBonusing();
    }
    protected void Shoot()
    {
        // �������� �� �������� ������
        _currentWeapon.Shoot(_damageMultiplier);
    }

    protected bool CheckHasBulletsInRow()
    {
        // ���������� ���� ������� ����
        return _currentWeapon.CheckHasBulletsInRow();
    }

    protected void Reload()
    {
        _currentWeapon.Reload();

        // �����: �������� ����� RefreshReloadingAnimation()
        RefreshReloadingAnimation();
    }

    private void InitWeapons(Weapon[] weapons)
    {
        // �������� �� ������� ������
        for (int i = 0; i < weapons.Length; i++)
        {
            // �������������� ������
            weapons[i].Init();
        }
    }
    public void SetWeapon(WeaponIdentity id)
    {
        // ���������� ����� ������
        _weaponId = id;

        // �������� ����� SetCurrentWeapon()
        SetCurrentWeapon(_weaponId);
    }
    // �����: �������� int id �� WeaponIdentity identity
    // �� ���� ������ ������ ������ ��������� ��� ��������
    private void SetCurrentWeapon(WeaponIdentity identity)
    {
        // �����: �������� ����� UnsubscriveFromEndReloading()
        UnsubscriveFromEndReloading();

        for (int i = 0; i < _weapons.Length; i++)
        {
            Weapon weapon = _weapons[i];
            bool isTargetId = weapon.Id == identity;
            if (isTargetId)
            {
                _currentWeapon = weapon;
                OnSetCurrentWeapon?.Invoke(weapon);

                // �����: �������� ����� SubscribeToEndReloading()
                SubscribeToEndReloading();
            }
            weapon.SetActive(isTargetId);
        }

        int id = WeaponIdentifier.GetAnimationIdByWeaponIdentify(identity);
        _animator.SetInteger(WeaponIdKey, id);

        // �����: �������� ����� RefreshReloadingAnimation()
        RefreshReloadingAnimation();
    }
    // ��������� �������� �����������
    private void RefreshReloadingAnimation()
    {
        // ���� ������� ������ ��������������
        // ������������� ��� ���������� �������� 0, ����� � 1
        // ��� ����� ��������, ��������, ��������� ������
        _rig.weight = _currentWeapon.IsReloading ? 0 : 1;

        // ������������� �������� ��������� �������� IsReloading
        // � ����������� �� ����, �������������� �� ������ ������
        _animator.SetBool(IsReloadingKey, _currentWeapon.IsReloading);
    }
    // ������������� �� ������� ��������� �����������
    private void SubscribeToEndReloading()
    {
        // ���� �������� ������ ���
        if (!_currentWeapon)
        {
            // ������� �� ������
            return;
        }
        // ������������ ������� OnEndReloading
        // �������� ����� RefreshReloadingAnimation()
        _currentWeapon.OnEndReloading += RefreshReloadingAnimation;
    }
    // ������������ �� ������� ��������� �����������
    private void UnsubscriveFromEndReloading()
    {
        // ���� �������� ������ ���
        if (!_currentWeapon)
        {
            // ������� �� ������
            return;
        }
        // ������������ �� ������� OnEndReloading
        _currentWeapon.OnEndReloading -= RefreshReloadingAnimation;
    }
    // ������������ �� �������� �����������
    // ����� ������ ���������
    private void OnDestroy()
    {
        // �������� ����� UnsubscriveFromEndReloading()
        UnsubscriveFromEndReloading();
    }
    protected void DamageBonusing()
    {
        // ���� ������������ ����� <= 0
        if (_damageMultiplierDuration <= 0)
        {
            // ������� �� ������
            return;
        }
        // ����������� ������ �����
        _damageMultiplierTimer += Time.deltaTime;

        // �������� ������� OnChangeDamageTimer
        // ������� � ���� ������ � ������������ �����
        OnChangeDamageTimer?.Invoke(_damageMultiplierTimer, _damageMultiplierDuration);

        // ���� �������� ������� ������ ������������
        if (_damageMultiplierTimer >= _damageMultiplierDuration)
        {
            // �������� ����� SetDefaultDamageMultiplier()
            SetDefaultDamageMultiplier();
        }
    }

   
    private void SetDefaultDamageMultiplier()
    {
        // ������������� ����������� ���������� �����
        SetDamageMultiplier(DefaultDamageMutiplier, 0);
    }

}