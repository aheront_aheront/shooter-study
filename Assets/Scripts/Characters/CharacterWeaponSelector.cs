// ���������� �������
using System;

using UnityEngine;

// ������� ����� �����������
public abstract class CharacterWeaponSelector : CharacterPart
{
    // ����� ������
    [SerializeField] protected WeaponIdentity _weaponId;

    // ������� ������ ������
    public Action<WeaponIdentity> OnWeaponSelected;

    // ��������� ����� ������
    public void RefreshSelectedWeapon()
    {
        // �������� ������� OnWeaponSelected
        // ������� � ���� ����� ������
        OnWeaponSelected?.Invoke(_weaponId);
    }
    // �������������� ����� OnInit() 
    protected override void OnInit()
    {
        // �������� ����� RefreshSelectedWeapon()
        RefreshSelectedWeapon();
    }

}
