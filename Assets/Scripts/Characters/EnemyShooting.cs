using UnityEngine;

public abstract class EnemyShooting : CharacterShooting
{
    [SerializeField] private float _shootingRange = 10f;

    private Transform _targetTransform;

    protected override void OnInit()
    {
        base.OnInit();
        _targetTransform = FindAnyObjectByType<Player>().transform;
    }

    // �����: �������������� ����� Shooting()
    protected override void Shooting()
    {
        // ���� ���� �������� � ���� ���� � ������
        if (CheckTargetInRange() && CheckHasBulletsInRow())
        {
            // ���� ��������
            Shoot();
        }
    }
    // �����: �������������� ����� Reloading()
    protected override void Reloading()
    {
        // ���� ��� ���� � ������
        if (!CheckHasBulletsInRow())
        {
            // ���� ������������ ������
            Reload();
        }
    }

    protected bool CheckTargetInRange()
    {
        return (_targetTransform.position - transform.position).magnitude <= _shootingRange;
    }
}