using UnityEngine;

// ���������� ��������
using UnityEngine.Animations.Rigging;

// ��������� ��������� IWeaponDependent
public abstract class CharacterAiming : CharacterPart, IWeaponDependent
{
    // ������ ��������� ������������
    private WeaponAiming[] _weaponAimings;

    // ���������� ��� ��������
    private RigBuilder _rigBuilder;

    // ����� ������
    private WeaponIdentity _weaponId;

    // ������������� ������
    public void SetWeapon(WeaponIdentity id)
    {
        // �������� ������� ������
        _weaponId = id;

        // �������� ����� SetCurrentWeapon()
        SetCurrentWeapon(_weaponId);
    }
    // �������������� ����������
    protected override void OnInit()
    {
        // �������� WeaponAiming �� �������� ��������
        _weaponAimings = GetComponentsInChildren<WeaponAiming>(true);

        // �������� RigBuilder �� �������� ��������
        _rigBuilder = GetComponentInChildren<RigBuilder>();
    }
    // �������� ������ � �������
    protected void InitWeaponAimings(Transform aim)
    {
        // �������� �� ���� ��������� _weaponAimings
        for (int i = 0; i < _weaponAimings.Length; i++)
        {
            // �������� � weaponAimings[i] ����� Init()
            // � ������� ��� ��������� ���� aim
            _weaponAimings[i].Init(aim);
        }
        // �������� � _rigBuilder ���������� ����� Build()
        // ����� ��������� ��������� �������� ���������
        _rigBuilder.Build();
    }
    // ������������� ������� ������
    private void SetCurrentWeapon(WeaponIdentity identity)
    {
        // �������� �� ���� ��������� _weaponAimings
        for (int i = 0; i < _weaponAimings.Length; i++)
        {
            // �������� ������� ������� ������������
            WeaponAiming weaponAiming = _weaponAimings[i];

            // ���������, ��� ��� ������ �������
            bool isTargetId = weaponAiming.Id == identity;

            // ������������� ���������� ������������
            weaponAiming.SetActive(isTargetId);
        }
    }
}