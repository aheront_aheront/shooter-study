using System;
using UnityEngine;

public abstract class CharacterHealth : CharacterPart
{
    // �������� ���� ������ ���������
    [SerializeField] private AudioClip _deathClip;

    // �������� �����
    private AudioSource _audioSource;
    // ������� ��� ������ ���������
    // �� ������� �� ����� � ���������� ���� CharacterHealth
    public Action<CharacterHealth> OnDieWithObject;

    // ������� ��� ��������� ����� ��������
    public Action OnAddHealthPoints;
    // ��������� � ������ ������ ���������
    private const string DeathKey = "Death";

    // ��������� ���������� ��������
    [SerializeField] private int _startHealthPoints = 100;

    // �������� ���������
    private Animator _animator;

    // ���� �������� ���������
    private int _healthPoints;

    // ���� ������ ���������
    private bool _isDead;

    // ������� ��� ������
    public event Action OnDie;
    public void AddHealthPoints(int value)
    {
        if (_isDead)
        {
            return;
        }

        _healthPoints += value;

        // �����: ���������, ��� �������� � �������� �� ���� �� ��������� ����������
        _healthPoints = Mathf.Clamp(_healthPoints, 0, _startHealthPoints);

        OnAddHealthPoints?.Invoke();

        if (_healthPoints <= 0)
        {
            Die();
        }
    }
    protected override void OnInit()
    {
        _animator = GetComponentInChildren<Animator>();

        // �����: �������� ��������� AudioSource
        _audioSource = GetComponentInChildren<AudioSource>();

        _healthPoints = _startHealthPoints;
        _isDead = false;
    }


    private void Die()
    {
        _isDead = true;
        _animator.SetTrigger(DeathKey);

        // �����: ������������� ���� ������
        _audioSource.PlayOneShot(_deathClip);

        OnDie?.Invoke();
        OnDieWithObject?.Invoke(this);
    }

    public int GetStartHealthPoints()
    {
        // ���������� ��������� �������� ��������
        return _startHealthPoints;
    }

    public int GetHealthPoints()
    {
        // ���������� ������� �������� ��������
        return _healthPoints;
    }
}
