using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    // ����� ���������
    private CharacterPart[] _parts;

    private void Start()
    {
        // �������� ����� Init()
        Init();
    }

    private void Init()
    {
        _parts = GetComponents<CharacterPart>();
        for (int i = 0; i < _parts.Length; i++)
        {
            _parts[i].Init();
        }
        InitDeath();

        // �����: �������� ����� InitWeaponSelection()
        InitWeaponSelection();
    }

    private void InitWeaponSelection()
    {
        // �������� �� ���� ������ ���������
        for (int i = 0; i < _parts.Length; i++)
        {
            // ���� ����� ����� ���� CharacterWeaponSelector
            if (_parts[i] is CharacterWeaponSelector weaponSelector)
            {
                // ������������ ������� OnWeaponSelected
                weaponSelector.OnWeaponSelected += SelectWeapon;

                // �������� ����� RefreshSelectedWeapon()
                weaponSelector.RefreshSelectedWeapon();
            }
        }
    }
    private void SelectWeapon(WeaponIdentity id)
    {
        // �������� �� ���� ������ ���������
        for (int i = 0; i < _parts.Length; i++)
        {
            // ���� ����� ����� � ����������� IWeaponDependent
            if (_parts[i] is IWeaponDependent weaponDependent)
            {
                // �������� ����� SetWeapon()
                weaponDependent.SetWeapon(id);
            }
        }
    }


    private void InitDeath()
    {
        // �������� �� ���� ������
        for (int i = 0; i < _parts.Length; i++)
        {
            // ���� ����� � ��������� ������ CharacterHealth
            if (_parts[i] is CharacterHealth health)
            {
                // ������������� �� ������� OnDie ������� health
                // ���������, ��� ��� ������������� ������� ������ ����������� ����� Stop()
                health.OnDie += Stop;
            }
        }
    }

    private void Stop()
    {
        // �������� �� ���� ������
        for (int i = 0; i < _parts.Length; i++)
        {
            // �������� ����� Stop() ��� ������ �����
            _parts[i].Stop();
        }
    }
}
