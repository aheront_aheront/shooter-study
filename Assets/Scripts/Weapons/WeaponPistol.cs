public class WeaponPistol : Weapon
{
    // Получаем номер пистолета
    public override WeaponIdentity Id => WeaponIdentity.Pistol;

    protected override void DoShoot(float damageMultiplier)
    {
        // Вызываем метод DefaultShoot()
        DefaultShoot(damageMultiplier);
    }
}