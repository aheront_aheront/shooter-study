// ���������� ���������
using System.Collections.Generic;

// ������� ����� ���������
public static class WeaponIdentifier
{
    // ������� ��������� ������
    private static Dictionary<WeaponIdentity, int> _weaponIdentityToAnimationIdPairs = new Dictionary<WeaponIdentity, int>
    {
        // �������� ������������� ����� 0
        {WeaponIdentity.Rifle, 0},

        // ��������� ������������� ����� 1
        {WeaponIdentity.Pistol, 1},

        // ��������� ������������� ����� 2
        {WeaponIdentity.Shotgun, 2}
    };

    // �������� �� ������ ������ ��� ��������
    public static int GetAnimationIdByWeaponIdentify(WeaponIdentity identity)
    {
        // ���������� ���������� ����� ��������
        return _weaponIdentityToAnimationIdPairs[identity];
    }
}

// ������ ����� ������
public enum WeaponIdentity
{
    // ��������
    Rifle,

    // ��������
    Pistol,

    // ��������
    Shotgun
}