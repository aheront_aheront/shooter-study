using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // ���� �� ����
    private int _damage;
    // ������, ������� ����� ���������� ��� ��������� ����
    [SerializeField] private GameObject _hitPrefab;

    // �������� ����
    [SerializeField] private float _speed = 30f;

    // ����� ����������� ���� �� ������
    [SerializeField] private float _lifeTime = 2f;
    // �������� ���� ������� ���������
    [SerializeField] private AudioClip _humanHitClip;

    // �������� ���� ����� �� ��������
    [SerializeField] private AudioClip _commonHitClip;

    // ���� ������� ���������
    private bool _isHumanHit;
    private void Update()
    {
        // ��������� ����� ����������� ���� �� ������
        ReduceLifeTime();

        // ��������� ��������� � ������
        CheckHit();

        // ���������� ����
        Move();
    }
    public void SetDamage(int value)
    {
        // ������ ���� ������ value
        _damage = value;
    }
    private void CheckPhysicObjectHit(RaycastHit hit)
    {
        // ������ ���������� ��� ������� �����������
        IPhysicHittable hittedPhysicObject = hit.collider.GetComponentInParent<IPhysicHittable>();

        // ���� ������ �� ������ (����������)
        if (hittedPhysicObject != null)
        {
            // �������� � ���� ����� ��������� Hit()
            // ������� ����������� ����, ���������� �� � ��������
            // � ����� �����, � ������� ������ ���� �� �������
            hittedPhysicObject.Hit(transform.forward * _speed, hit.point);
        }
    }

    private void ReduceLifeTime()
    {
        // ��������� ����� ����������� ���� �� �����, ��������� � ���������� �����
        _lifeTime -= Time.deltaTime;

        // ���� ����� ����������� ���� �������
        if (_lifeTime <= 0)
        {
            // ���� ��������� � ������
            DestroyBullet();
        }
    }

    private void CheckHit()
    {
        // �����: �������� && !hit.collider.isTrigger
        // ����� ���� �� �������� � ���������� �������
        if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, _speed * Time.deltaTime)
            && !hit.collider.isTrigger)
        {
            Hit(hit);
        }
    }

    private void Move()
    {
        // ������ ������� ���� ����� ��������� �������� � �������
        transform.position += transform.forward * _speed * Time.deltaTime;
    }
    private void Hit(RaycastHit hit)
    {
        CheckCharacterHit(hit);
        CheckPhysicObjectHit(hit);

        // �����: ������� ������ ��������� ����
        GameObject hitSample = Instantiate(_hitPrefab, hit.point, Quaternion.LookRotation(-transform.up, -transform.forward));

        // �����: �������� ����� PlaySound()
        PlaySound(hitSample, _isHumanHit);

        DestroyBullet();
    }

    private void CheckCharacterHit(RaycastHit hit)
    {
        CharacterHealth hittedHealth = hit.collider.GetComponentInChildren<CharacterHealth>();

        if (hittedHealth)
        {
            hittedHealth.AddHealthPoints(-_damage);

            // �����: ������ ���� ������� ���������
            _isHumanHit = true;
        }
    }
    private void PlaySound(GameObject hit, bool isHumanHit)
    {
        // �������� ��������� AudioSource
        AudioSource audioSource = hit.GetComponentInChildren<AudioSource>();

        // ������������� ���� ������� ��������� ��� ����� � �������
        audioSource.PlayOneShot(isHumanHit ? _humanHitClip : _commonHitClip);
    }

    private void DestroyBullet()
    {
        // ������� ������ ����
        Destroy(gameObject);
    }
}
