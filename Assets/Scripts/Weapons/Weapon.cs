using UnityEngine;
// ���������� �������
using System;

// �������� �� ���������� ����������
using Random = UnityEngine.Random;

// ������� ����� �����������
public abstract class Weapon : MonoBehaviour
{
    // ������� �����������
    public Action<int, int> OnBulletsInRowChange;
    // ���� �� ������
    [SerializeField] private int _damage = 10;
    // �������� ����� ������
    public abstract WeaponIdentity Id { get; }
    // ������ ����
    [SerializeField] private Bullet _bulletPrefab;

    // �������� ����� ����������
    [SerializeField] private float _bulletDelay = 0.05f;

    // ���������� ���� � ������
    [SerializeField] private int _bulletsInRow = 7;

    // ����������������� �����������
    [SerializeField] private float _reloadingDuration = 4f;
    // ������� ��������� �����������
    public Action OnEndReloading;

    // ���� ����, ��� ������ ��� �����������
    // ������� ������ � ���������� ���� �����
    public bool IsReloading => _isReloading;
    // ���� ������
    private WeaponSound _weaponSound;


    // ���� ��������������� ����
    [SerializeField] private float _spreadAngle = 5f;

    // ����� ��������� ����
    private Transform _bulletSpawnPoint;

    // ������� ���������� ���� � ������
    private int _currentBulletsInRow;

    // ������� ������� ����� ����������
    private float _bulletTimer;

    // ������� ������� �����������
    private float _reloadingTimer;

    // ���� ����� ��������
    private bool _isShootDelayEnd;

    // ���� �����������
    private bool _isReloading;
    public void Init()
    {
        _bulletSpawnPoint = GetComponentInChildren<BulletSpawnPoint>().transform;

        // �����: �������� ��������� WeaponSound
        _weaponSound = GetComponentInChildren<WeaponSound>();

        // �����: �������� � _weaponSound ����� Init()
        _weaponSound.Init();

        FillBulletsToRow();
    }
    public void Shoot(float damageMultiplier)
    {
        if (!_isShootDelayEnd || !CheckHasBulletsInRow())
        {
            return;
        }
        _bulletTimer = 0;

        // �����: �������� ����� DoShoot()
        DoShoot(damageMultiplier);
        _currentBulletsInRow--;
        // �������� ������� OnBulletsInRowChange
        // ������� � ���� ������� � ������� ����� ����
        OnBulletsInRowChange?.Invoke(_currentBulletsInRow, _bulletsInRow);
        // �������� ����� PlaySound() �� ������ ��������
        _weaponSound.PlaySound(SoundType.Shoot);
    }
    public void Reload()
    {
        // ���� ����������� � ��������
        if (_isReloading)
        {
            // ��������� ������ �����������
            _reloadingTimer += Time.deltaTime;

            // ������� �� ������
            return;
        }
        // ������ ���� �����������
        _isReloading = true;
        // �������� ����� PlaySound() �� ������ �����������
        _weaponSound.PlaySound(SoundType.Reload);
    }
    public bool CheckHasBulletsInRow()
    {
        // ��������� ���������� ���� � ������
        // ���� ��� > 0, ���������� true
        return _currentBulletsInRow > 0;
    }
    private void SpawnBullet(Bullet prefab, Transform spawnPoint, float damageMultiplier)
    {
        Bullet bullet = Instantiate(prefab, spawnPoint.position, spawnPoint.rotation);

        // �����: �������� ������� ���� ������� ����
        Vector3 bulletEulerAngles = bullet.transform.eulerAngles;

        // �����: �������� ���� ������� ���� �� ��� X
        // �� ��������� �������� � �������� ���� �����������
        bulletEulerAngles.x += Random.Range(-_spreadAngle, _spreadAngle);

        // �����: �������� ���� ������� ���� �� ��� Y
        // �� ��������� �������� � �������� ���� �����������
        bulletEulerAngles.y += Random.Range(-_spreadAngle, _spreadAngle);

        // �����: ��������� ���������� ���� ������� � ����,
        // ����� ��� ������ � ����� �����������
        bullet.transform.eulerAngles = bulletEulerAngles;

        InitBullet(bullet, damageMultiplier);
    }

    private void InitBullet(Bullet bullet, float damageMultiplier)
    {
        // ����� ���� �� ����
        bullet.SetDamage((int)(_damage * damageMultiplier));
    }
    private void Update()
    {
        // �������� ����� ShootDelaying()
        ShootDelaying();

        // �������� ����� Reloading()
        Reloading();
    }

    private void ShootDelaying()
    {
        // ��������� ������ ��������
        _bulletTimer += Time.deltaTime;

        // ������ �������� ����� ����� ��������
        // � ����������� �� �������� _bulletTimer >= _bulletDelay
        _isShootDelayEnd = _bulletTimer >= _bulletDelay;
    }
    // ����������� ����� ��������
    protected abstract void DoShoot(float damageMultiplier);

    // ����� �������� �� ���������
    protected void DefaultShoot(float damageMultiplier)
    {
        // ���������� ����
        SpawnBullet(_bulletPrefab, _bulletSpawnPoint, damageMultiplier);
    }
    private void Reloading()
    {
        // ���� ����� ���� �����������
        if (_isReloading)
        {
            // ����������� ������ �����������
            _reloadingTimer += Time.deltaTime;

            if (_reloadingTimer >= _reloadingDuration)
            {
                FillBulletsToRow();

                // �������� ������� OnEndReloading
                OnEndReloading?.Invoke();
            }
        }
    }
    private void FillBulletsToRow()
    {
        // ������� ���� �����������
        _isReloading = false;

        // �������� ������ �����������
        _reloadingTimer = 0;

        // ����� ������� ���������� ���� � ������
        _currentBulletsInRow = _bulletsInRow;
        // �������� ������� OnBulletsInRowChange
        // ������� � ���� ������� � ������� ����� ����
        OnBulletsInRowChange?.Invoke(_currentBulletsInRow, _bulletsInRow);
    }

    // ��������� �������� Damage
    // �������� �������� ��������� �������� _damage


    public void SetActive(bool value)
    {
        gameObject.SetActive(value);
        OnBulletsInRowChange?.Invoke(_currentBulletsInRow, _bulletsInRow);

        // �����: ���� �������� �������� value �������
        if (value)
        {
            // �����: �������� � _weaponSound ����� PlaySound()
            // �� ������ ����� ������
            _weaponSound.PlaySound(SoundType.Switch);
        }
    }
}