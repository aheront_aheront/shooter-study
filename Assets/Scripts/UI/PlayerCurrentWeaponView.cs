using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerCurrentWeaponView : MonoBehaviour
{
    // ������ �������� ������
    [SerializeField] private Image _iconImage;

    // ������� � ���������� ��������
    [SerializeField] private TextMeshProUGUI _bulletsText;

    // ������ ��������
    [SerializeField] private Sprite _rifleSprite;

    // ������ ���������
    [SerializeField] private Sprite _pistolSprite;

    // ������ ���������
    [SerializeField] private Sprite _shotgunSprite;

    // ������� ��� �������-������
    private Dictionary<WeaponIdentity, Sprite> _weaponToSpritePairs;

    // ������ ������ ������
    private CharacterWeaponSelector _playerWeaponSelector;

    // ������ ��������
    private CharacterShooting _playerShooting;

    // ������ ������
    private Weapon _playerWeapon;

    private void Awake()
    {
        // �������� ����� Init()
        Init();
    }

    private void Init()
    {
        // �������������� �������
        _weaponToSpritePairs = new Dictionary<WeaponIdentity, Sprite>()
    {
        // ��������� ������ �������� � � ��������
        { WeaponIdentity.Rifle, _rifleSprite },

        // ��������� ������ ��������� � ��� ��������
        { WeaponIdentity.Pistol, _pistolSprite },

        // ��������� ������ ��������� � ��� ��������
        { WeaponIdentity.Shotgun, _shotgunSprite }
    };
        // ������� ������ ���� PlayerWeaponSelector
        // ����������� ��� _playerWeaponSelector
        _playerWeaponSelector = FindObjectOfType<PlayerWeaponSelector>();

        // ������� ������ ���� PlayerShooting
        // ����������� ��� _playerShooting
        _playerShooting = FindObjectOfType<PlayerShooting>();

        // ������������ ������� OnWeaponSelected
        // �������� ����� SetIconByType()
        _playerWeaponSelector.OnWeaponSelected += SetIconByType;

        // ������������ ������� OnSetCurrentWeapon
        // �������� ����� SubscribeForBullets()
        _playerShooting.OnSetCurrentWeapon += SubscribeForBullets;
    }
    private void SubscribeForBullets(Weapon weapon)
    {
        // ������������ ������� OnBulletsInRowChange
        // �������� ����� SetBulletText()
        weapon.OnBulletsInRowChange += SetBulletText;

        // ���� ��� ���� ������ ������ ������
        if (_playerWeapon)
        {
            // ������������ �� ������� OnBulletsInRowChange
            _playerWeapon.OnBulletsInRowChange -= SetBulletText;
        }
        // ������������ ������ ������ � weapon
        _playerWeapon = weapon;
    }
    private void SetIconByType(WeaponIdentity weaponId)
    {
        // ������������� ������ �������� ������
        _iconImage.sprite = _weaponToSpritePairs[weaponId];
    }

    private void SetBulletText(int currentBulletsInRow, int bulletsInRow)
    {
        // ������������� ����� � ���������� ����
        _bulletsText.text = $"{currentBulletsInRow}/{bulletsInRow}";
    }

    private void OnDestroy()
    {
        // ���� ��� ���� ������ ������ ������ ������
        if (_playerWeaponSelector)
        {
            // ������������ �� ������� OnWeaponSelected
            _playerWeaponSelector.OnWeaponSelected -= SetIconByType;
        }
        // ���� ��� ���� ������ �������� ������
        if (_playerShooting)
        {
            // ������������ �� ������� OnSetCurrentWeapon
            _playerShooting.OnSetCurrentWeapon -= SubscribeForBullets;
        }
        // ���� ��� ���� ������ ������ ������
        if (_playerWeapon)
        {
            // ������������ �� ������� OnBulletsInRowChange
            _playerWeapon.OnBulletsInRowChange -= SetBulletText;
        }
    }
}
