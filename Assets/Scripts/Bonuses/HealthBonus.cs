using UnityEngine;

// ������� ������������ �� Bonus
public class HealthBonus : Bonus
{
    // ���������� ��������, ������� ��������� ������
    [SerializeField] private int _health = 50;

    // ������ �������� ������
    private CharacterHealth _healingCharacterHealth;

    // �������������� ����� CheckTriggeredObject()
    protected override bool CheckTriggeredObject(Collider other)
    {
        // ���� �� �������, � ������� �����������, ��������� PlayerHealth
        // �� ���� ���������, ��� ����� �������� �����
        _healingCharacterHealth = other.GetComponentInParent<PlayerHealth>();

        // ���������� true, ���� ����� PlayerHealth
        // ����� ���������� false
        return _healingCharacterHealth != null;
    }
    // �������������� ����� ApplyBonus()
    protected override void ApplyBonus()
    {
        // ��������� ������ ���� ��������
        _healingCharacterHealth.AddHealthPoints(_health);
    }
}