using UnityEngine;

// ������� ������������ �� Bonus
public class DamageBonus : Bonus
{
    // ��������� �����
    [SerializeField] private float _damageMultiplier = 2f;

    // ������������ �������� ������
    [SerializeField] private float _duration = 10f;

    // ������ �������� � �������
    private CharacterShooting _bonusedCharacterShooting;

    // �������������� ����� CheckTriggeredObject()
    protected override bool CheckTriggeredObject(Collider other)
    {
        // ���� �� �������, � ������� �����������, ��������� PlayerShooting
        // �� ���� ���������, ��� ����� �������� �����
        _bonusedCharacterShooting = other.GetComponentInParent<PlayerShooting>();

        // ���������� true, ���� ����� PlayerShooting
        // ����� ���������� false
        return _bonusedCharacterShooting != null;
    }
    // �������������� ����� ApplyBonus()
    protected override void ApplyBonus()
    {
        // ����������� ���� ���������
        // �� ���� ������, ������� �������� �����
        _bonusedCharacterShooting.SetDamageMultiplier(_damageMultiplier, _duration);
    }
}